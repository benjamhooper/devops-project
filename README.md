# DevOps Project

The goal of this project is to show an end-to-end CI/CD pipeline utilizing GitLab, Octopus Deploy, and Azure App Services.

## GitLab Pipeline

The following varibles are stored within the GitLab UI as they are sensitive and need to be protected to prevent secret leak:

* $OCTOPUS_SERVER
* $OCTOPUS_APIKEY
* $SPACE_ID
